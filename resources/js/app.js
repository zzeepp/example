import {createApp} from "vue";
import Roott       from "./components/Roott.vue";

require('./bootstrap');

const app = createApp(Roott);
app.mount('#app');