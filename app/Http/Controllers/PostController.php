<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\StorePostRequest;
use App\Http\Resources\Post\IndexPostResource;
use App\Models\Image;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller {
	public function store( StorePostRequest $request ) {
		$data   = $request->validated();
		$images = $data[ 'images' ];
		unset( $data[ 'images' ] );
		$post = Post::firstOrCreate( $data );
		foreach( $images as $image ) {
			$name     = md5( Carbon::now() . '_' . $image->getClientOriginalName() ) . '.' . $image->getClientOriginalExtension();
			$prevName = 'prev_' . $name;
			\Intervention\Image\Facades\Image::make( $image )
											 ->fit( 100,
													100 )
											 ->save( storage_path( 'app/public/images/' . $prevName ) );
			$filePath = Storage::disk( 'public' )
							   ->putFileAs( '/images',
											$image,
											$name );
			Image::create( [
							   'path'        => $filePath,
							   'url'         => url( '/storage/' . $filePath ),
							   'preview_url' => url( '/storage/images/' . $prevName ),
							   'post_id'     => $post->id,
						   ] );
			
		}
		return response()->json( [ 'message' => 'success', ] );
	}
	
	public function index() {
		$post = Post::latest()
					->first();
		return new IndexPostResource( $post );
	}
}
