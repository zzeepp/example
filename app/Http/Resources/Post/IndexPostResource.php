<?php

namespace App\Http\Resources\Post;

use App\Http\Resources\Image\IndexImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexPostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
		return [
			'id'     => $this->id,
			'title'  => $this->title,
			'images' => IndexImageResource::collection($this->images),
		];
    }
}
